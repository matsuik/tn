# data
### toytreeをrotateした.ipynb
めっちゃ汚い  
skimage.trainsform.ronteでtoytreeのfeatureImg, GT, maskを回転



# RCN
### TNN_analytical_sol
linearの解析解のweightの性質を見る

### TN_ab_neuron.ipynb
TNN.abで神経画像を本気で学習

### TN_ab_toytree12.ipynb
TNN.abでtoytreeのfeatureの12から23を使って学習

# VNN conditional number

### CondNumLinear.ipynb
cn_invの計算がうまく行ってることを確認

### CondNumVLR.ipynb
vector linear regressionでfeatureの１２から23を使って学習
cn_invでweightの性質をみる。

### CondNumDNN.ipynb
theano_dnnにtoytreeを回転させたやつをいっぱい与えて学習
cn_invを見る
