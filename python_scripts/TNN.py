# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 17:43:44 2015

@author: matsuik
"""

from __future__ import division

import numpy as np

import theano
import theano.tensor as T



def rtheta(featureImg, imgGT, n_hidden, h_activation, out_activation):
    """
    featureImg (n_pixels, n_tensor, 2)
    imgGT (n_pixels, 2)
    """
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True)
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "gt2", borrow=True)
    
    s_reg2 = T.scalar()
    s_reg1 = T.scalar()
    
    s_lr = T.scalar()
    
    batch_size = T.iscalar()
    
    # 1つのデータ (batch, 60, 2)の代わり
    x_sy = T.tensor3("x")
    # １つのtarget (batch, 2)の代わり
    t_sy = T.matrix("y")
    
    if h_activation == "sigmoid":
        activate = T.nnet.sigmoid
    elif h_activation == "ReLU":
        def activate(x):
            return T.switch(x < 0, 0, x)
       
### ここまで共通
    
    hidden_list = []
    magni_list = []
    theta_list = []
    bias_list = []

    
### 1段目    
    for i_hidden in range(n_hidden):
        # 60個のr
        magni = theano.shared(np.random.uniform(low=-0.01, high=0.01,
                                                size=(n_tensor, )))
        # 60個のtheta
        theta = theano.shared(np.random.uniform(low=-np.pi, high=np.pi,
                                                size=(n_tensor, )))
        bias = theano.shared(0.)
        
        # ６０この回転伸縮行列
        cos = T.cos(theta)
        sin = T.sin(theta)
        rot = T.as_tensor([cos, -sin, sin, cos]) # (4, n_tensor)
        orth = abs(magni) * rot # (4, n_tensor)
        orth_t = orth.T # (n_tensor, 4)
        w_mats = orth_t.reshape((n_tensor, 2, 2))
        
        
        # (batch, 2) <- (batch_size, n_tensor, 2) * (n_tensor, 2, 2)
        h = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
        
        out = activate(h.norm(L=2, axis=1) + bias).dimshuffle(0, "x") \
            * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

        hidden_list.append(out) # (n_hidden, batch, 2)
        magni_list.append(magni)
        theta_list.append(theta)
        bias_list.append(bias)
    
    
    hiddens = T.stacklists(hidden_list).dimshuffle(1, 0, 2) # (batch, n_hidden, 2)
    
### 2段目
    # 60個のr
    magni2 = theano.shared(np.random.uniform(low=-0.01, high=0.01,
                                             size=(n_hidden, )))
    # 60個のtheta
    theta2 = theano.shared(np.random.uniform(low=-np.pi, high=np.pi,
                                             size=(n_hidden, )))                                                

    # ６０この回転伸縮行列
    cos = T.cos(theta2)
    sin = T.sin(theta2)
    rot = T.as_tensor([cos, -sin, sin, cos]) # (4, n_hidden)
    orth = abs(magni2) * rot # (4, n_hidden)
    orth_t = orth.T # (n_hidden, 4)
    w_mats = orth_t.reshape((n_hidden, 2, 2))
        
    # (batch, 2) <- (batch_size, n_hidden, 2) * (n_hidden, 2, 2)
    if out_activation == "linear":
        param2_list = [magni2, theta2]
        out = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
    if out_activation == "sigmoid":
        bias2 = theano.shared(0.)
        param2_list = [magni2, theta2, bias2]
        h = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
        out = T.nnet.sigmoid(h.norm(L=2, axis=1) + bias2).dimshuffle(0, "x") \
            * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    
#　costの定義と最適化

    cost = T.sum((out - t_sy)**2)
    magniL2 = T.sum([m.norm(L=2) for m in magni_list])
    magniL1 = T.sum([m.norm(L=1) for m in magni_list])
    obj = cost + s_reg2*(magni2.norm(L=2) + magniL2) + s_reg1*(magni2.norm(L=1) + magniL1)
    
### こっからも共通にしたい
    
    grad2_list = [T.grad(cost=obj, wrt=param) for param in param2_list]
    magni_grad_list = [T.grad(cost=obj, wrt=m) for m in magni_list]
    theta_grad_list = [T.grad(cost=obj, wrt=t) for t in theta_list]
    bias_grad_list = [T.grad(cost=obj, wrt=b) for b in bias_list]
    
    #delta_list = [-s_lr / batch_size * grads for grads in grad_list]
    delta2_list = [-s_lr/batch_size * grads for grads in grad2_list]
    magni_delta_list = [-s_lr/batch_size * grads for grads in magni_grad_list]
    theta_delta_list = [-s_lr/batch_size * grads for grads in theta_grad_list]
    bias_delta_list = [-s_lr/batch_size * grads for grads in bias_grad_list]
    
    updated2_list = [param + delta for param, delta in zip(param2_list, delta2_list)]
    magni_updated_list = [param + delta for param, delta in zip(magni_list, magni_delta_list)]
    theta_updated_list = [param + delta for param, delta in zip(theta_list, theta_delta_list)]
    bias_updated_list = [param + delta for param, delta in zip(bias_list, bias_delta_list)]
    
    updates = zip(param2_list, updated2_list) + zip(magni_list, magni_updated_list) + \
        zip(theta_list, theta_updated_list) + zip(bias_list, bias_updated_list)
    
    
    index = T.iscalar("index")
    
    
    f_train = theano.function(
        inputs=[index, s_lr, s_reg2, s_reg1, batch_size],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])],
        profile=True
        )
        
    f_train_cost = theano.function(
        inputs=[index, s_lr, s_reg2, s_reg1, batch_size],
        outputs=[cost],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])],
        profile=True
        )
        
    f_output = theano.function(
        inputs=[x_sy],
        outputs=[out],
        profile=True
        )
        
    return f_train, f_train_cost, f_output, s_input, s_target, param2_list, \
                magni_list, theta_list, bias_list
    
    
def ab(featureImg, imgGT, target_featureImg, target_imgGT,
       n_hidden, h_activation, out_activation):
    """
    Parameters
    -----------
    
    featureImg : ndarray
        (n_pixels, n_tensor, 2)
    imgGT : ndarray
        (n_pixels, 2)
    target_featureImg, target_imgGT
    n_hidden : int
    h_activation : str
        "sigm" or "ReLU"
    out_activation : str
        "sigm" , "ReLU" or "linear"
    """
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True)
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "gt2", borrow=True)
                             
    s_test_input = theano.shared(np.asarray(target_featureImg, dtype=theano.config.floatX),
                                 "target_featureImg", borrow=True)
    s_test_target = theano.shared(np.asarray(target_imgGT, dtype=theano.config.floatX),
                                  "target_imgGT", borrow=True)
    
    
    s_reg2 = T.scalar()
    
    s_lr = T.scalar()
    
    batch_size = T.iscalar()
    
    hidden_list = []
    a_list = []
    b_list = []
    bias_list = []
    
    # 1つのデータ (batch, 60, 2)の代わり
    x_sy = T.tensor3("x")
    # １つのtarget (batch, 2)の代わり
    t_sy = T.matrix("y")
    
    if h_activation == "sigmoid":
        activate = T.nnet.sigmoid
    elif h_activation == "ReLU":
        def activate(x):
            return T.switch(x < 0, 0, x)

    
### 1段目    
    for i_hidden in range(n_hidden):
        # 60個のa
        a = theano.shared(np.asarray(0.01*np.random.normal(size=(n_tensor, )),
                                     dtype=theano.config.floatX))
        # 60個のb
        b = theano.shared(np.asarray(0.01*np.random.normal(size=(n_tensor, )),
                                     dtype=theano.config.floatX))
        bias = theano.shared(0.)
        
        # ６０この回転伸縮行列
        orth = T.as_tensor([a, -b, b, a]) # (4, n_tensor)
        orth_t = orth.T # (n_tensor, 4)
        w_mats = orth_t.reshape((n_tensor, 2, 2))
        
        
        # (batch, 2) <- (batch_size, n_tensor, 2) * (n_tensor, 2, 2)
        h = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
        
        out = activate(h.norm(L=2, axis=1) + bias).dimshuffle(0, "x") \
        * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

        hidden_list.append(out) # (n_hidden, batch, 2)
        a_list.append(a)
        b_list.append(b)
        bias_list.append(bias)
    
    
    hiddens = T.stacklists(hidden_list).dimshuffle(1, 0, 2) # (batch, n_hidden, 2)
    
### 2段目
    # 60個のa2
    a2 = theano.shared(np.asarray(0.01*np.random.normal(size=(n_hidden, )),
                                  dtype=theano.config.floatX))
    # 60個のb2
    b2 = theano.shared(np.asarray(0.01*np.random.normal(size=(n_hidden, )),
                                  dtype=theano.config.floatX))   

    # ６０この回転伸縮行列
    orth = T.as_tensor([a2, -b2, b2, a2]) # (4, n_hidden)
    orth_t = orth.T # (n_hidden, 4)
    w_mats = orth_t.reshape((n_hidden, 2, 2))
        
    # (batch, 2) <- (batch_size, n_hidden, 2) * (n_hidden, 2, 2)
    if out_activation == "linear":
        param2_list = [a2, b2]
        out = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
    if out_activation == "ReLU":
        bias2 = theano.shared(0.)
        param2_list = [a2, b2, bias2]
        h = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
        def ReLU(x):
            return T.switch(x < 0, 0, x)
        out = ReLU(h.norm(L=2, axis=1) + bias2).dimshuffle(0, "x") * h\
        / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    if out_activation == "sigmoid":
        bias2 = theano.shared(0.)
        param2_list = [a2, b2, bias2]
        h = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
        out = T.nnet.sigmoid(h.norm(L=2, axis=1) + bias2).dimshuffle(0, "x")\
    * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    
#　costの定義と最適化

    cost = T.mean((out - t_sy)**2)
    aL2 = T.sum([m.norm(L=2) for m in a_list])
    bL2 = T.sum([m.norm(L=2) for m in b_list])
    obj = cost + s_reg2*(a2.norm(L=2) + b2.norm(L=2) + aL2 + bL2)
    
    grad2_list = [T.grad(cost=obj, wrt=param) for param in param2_list]
    a_grad_list = [T.grad(cost=obj, wrt=m) for m in a_list]
    b_grad_list = [T.grad(cost=obj, wrt=t) for t in b_list]
    bias_grad_list = [T.grad(cost=obj, wrt=bi) for bi in bias_list]
    
    #delta_list = [-s_lr / batch_size * grads for grads in grad_list]
    delta2_list = [-s_lr/batch_size * grads for grads in grad2_list]
    a_delta_list = [-s_lr/batch_size * grads for grads in a_grad_list]
    b_delta_list = [-s_lr/batch_size * grads for grads in b_grad_list]
    bias_delta_list = [-s_lr/batch_size * grads for grads in bias_grad_list]
    
    updated2_list = [param + delta for param, delta in zip(param2_list, delta2_list)]
    a_updated_list = [param + delta for param, delta in zip(a_list, a_delta_list)]
    b_updated_list = [param + delta for param, delta in zip(b_list, b_delta_list)]
    bias_updated_list = [param + delta for param, delta in zip(bias_list, bias_delta_list)]
    
    updates = zip(param2_list, updated2_list) + zip(a_list, a_updated_list) + \
        zip(b_list, b_updated_list) + zip(bias_list, bias_updated_list)
    
    
    index = T.iscalar("index")
    
    
    f_train = theano.function(
        inputs=[index, s_lr, s_reg2, batch_size],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])]
        )
    
    f_training_error = theano.function(
        inputs=[],
        outputs=[cost],
        givens=[(x_sy, s_input),
                (t_sy, s_target)]
        )
        
    f_test_error = theano.function(
        inputs=[],
        outputs=[cost],
        givens=[(x_sy, s_test_input),
                (t_sy, s_test_target)]
        )
    
    f_output = theano.function(
        inputs=[x_sy],
        outputs=[out]
        )
        
    result = [f_train, f_training_error, f_test_error, f_output, s_input,
              s_target, param2_list, a_list, b_list, bias_list]
        
    return result
    
    
def diag(featureImg, imgGT, target_featureImg, target_imgGT,
       n_hidden, h_activation, out_activation):
    """
    Parameters
    -----------
    
    featureImg : ndarray
        (n_pixels, n_tensor, 2)
    imgGT : ndarray
        (n_pixels, 2)
    target_featureImg, target_imgGT
    n_hidden : int
    h_activation : str
        "sigm" or "ReLU"
    out_activation : str
        "sigm" , "ReLU" or "linear"
    """
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True)
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "gt2", borrow=True)
                             
    s_test_input = theano.shared(np.asarray(target_featureImg, dtype=theano.config.floatX),
                            "target_featureImg", borrow=True)
    s_test_target = theano.shared(np.asarray(target_imgGT, dtype=theano.config.floatX),
                             "target_imgGT", borrow=True)
    
    
    s_reg2 = T.scalar()
    
    s_lr = T.scalar()
    
    batch_size = T.iscalar()
    
    # 1つのデータ (batch, 60, 2)の代わり
    x_sy = T.tensor3("x")
    # １つのtarget (batch, 2)の代わり
    t_sy = T.matrix("y")
    
    if h_activation == "sigmoid":
        activate = T.nnet.sigmoid
    elif h_activation == "ReLU":
        def activate(x):
            return T.switch(x<0, 0, x)

    hidden_list = []
    a_list = []
    bias_list = []

    
### 1段目    
    for i_hidden in range(n_hidden):
        # 60個のa
        a = theano.shared(np.asarray(0.01*np.random.normal(size=(n_tensor, )),
                                      dtype=theano.config.floatX))
        
        bias = theano.shared(0.)
        
        # ６０この回転伸縮行列
        orth = T.as_tensor([a, np.zeros(shape=(n_tensor,)), np.zeros(shape=(n_tensor,)), a]) # (4, n_tensor)
        orth_t = orth.T # (n_tensor, 4)
        w_mats = orth_t.reshape((n_tensor, 2, 2))
        
        
        # (batch, 2) <- (batch_size, n_tensor, 2) * (n_tensor, 2, 2)
        h = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
        
        out = activate(h.norm(L=2, axis=1) + bias).dimshuffle(0, "x") * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

        hidden_list.append(out) # (n_hidden, batch, 2)
        a_list.append(a)
        bias_list.append(bias)
    
    
    hiddens = T.stacklists(hidden_list).dimshuffle(1, 0, 2) # (batch, n_hidden, 2)
    
### 2段目
    # 60個のa2
    a2 = theano.shared(np.asarray(0.01*np.random.normal(size=(n_hidden, )),
                                  dtype=theano.config.floatX))
    orth = T.as_tensor([a2, np.zeros(shape=(n_hidden,)), np.zeros(shape=(n_hidden,)), a2]) # (4, n_hidden)
    orth_t = orth.T # (n_hidden, 4)
    w_mats = orth_t.reshape((n_hidden, 2, 2))
        
    # (batch, 2) <- (batch_size, n_hidden, 2) * (n_hidden, 2, 2)
    if out_activation == "linear":
        param2_list = [a2]
        out = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
    if out_activation == "ReLU":
        bias2 = theano.shared(0.)
        param2_list = [a2, bias2]
        h = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
        def ReLU(x):
            return T.switch(x<0, 0, x)
        out = ReLU(h.norm(L=2, axis=1) + bias2).dimshuffle(0, "x") * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    if out_activation == "sigmoid":
        bias2 = theano.shared(0.)
        param2_list = [a2, bias2]
        h = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
        out = T.nnet.sigmoid(h.norm(L=2, axis=1) + bias2).dimshuffle(0, "x") * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    
#　costの定義と最適化

    cost = T.mean((out - t_sy)**2)
    aL2 = T.sum([m.norm(L=2) for m in a_list])
    obj = cost + s_reg2*(a2.norm(L=2) + aL2)
    
###
    
    grad2_list = [T.grad(cost=obj, wrt=param) for param in param2_list]
    a_grad_list = [T.grad(cost=obj, wrt=m) for m in a_list]
    bias_grad_list = [T.grad(cost=obj, wrt=bi) for bi in bias_list]
    
    #delta_list = [-s_lr / batch_size * grads for grads in grad_list]
    delta2_list = [-s_lr/batch_size * grads for grads in grad2_list]
    a_delta_list = [-s_lr/batch_size * grads for grads in a_grad_list]
    bias_delta_list = [-s_lr/batch_size * grads for grads in bias_grad_list]
    
    updated2_list = [param + delta for param, delta in zip(param2_list, delta2_list)]
    a_updated_list = [param + delta for param, delta in zip(a_list, a_delta_list)]
    bias_updated_list = [param + delta for param, delta in zip(bias_list, bias_delta_list)]
    
    updates = zip(param2_list, updated2_list) + zip(a_list, a_updated_list) + \
         zip(bias_list, bias_updated_list)
    
    
    index = T.iscalar("index")
    
    
    f_train = theano.function(
        inputs=[index, s_lr, s_reg2, batch_size],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])]
        )
    
    f_training_error = theano.function(
        inputs=[],
        outputs=[cost],
        givens=[(x_sy, s_input),
                (t_sy, s_target)]
        )
        
    f_test_error = theano.function(
        inputs=[],
        outputs=[cost],
        givens=[(x_sy, s_test_input),
                (t_sy, s_test_target)]
        )
    
    f_output = theano.function(
        inputs=[x_sy],
        outputs=[out]
        )
        
    result = [f_train, f_training_error, f_test_error, f_output, s_input, s_target, param2_list, a_list, bias_list]
        
    return result