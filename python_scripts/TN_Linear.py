# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 15:56:00 2015

@author: matsuik
"""

from __future__ import division

import time

import numpy as np

import theano
import theano.tensor as T

def train(featureImg, imgGT, lr, mom, reg2, reg1, n_epochs,
          w=None):
    """
    featureImg (n_pixels, n_tensor, 2)
    imgGT (n_pixels, 2)
    """
    
    # データの正規化
    norm_coef = np.std(np.linalg.norm(featureImg, axis=2), axis=0) # (60,)
    featureImg = featureImg / norm_coef[:, np.newaxis]
    
    gt2 = np.zeros(imgGT.shape)
    gt2[:, 0] = imgGT[:,0]**2 - imgGT[:,1]**2
    gt2[:, 1] = 2 * imgGT[:,0] * imgGT[:,1]
    
    gt2 = gt2 / (np.linalg.norm(imgGT, axis=1) + 0.0001)
    
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True,
                            )
    s_target = theano.shared(np.asarray(gt2, dtype=theano.config.floatX),
                             "gt2", borrow=True,
                             )
                             
    s_reg2 = theano.shared(0.)
    s_reg1 = theano.shared(0.)
                             
    s_mom = theano.shared(mom)
    
    s_lr = theano.shared(lr)
    
    # 120個のパラメータ
    
    if w == None:
        r = np.random.uniform(low=-0.1, high=0.1, size=(60, ))
        theta = np.random.uniform(low=0.0, high=2*np.pi, size=(60, ))
        w = np.zeros(120)
        w[::2] = r * np.cos(theta)
        w[1::2] = r * np.sin(theta)
    w = theano.shared(w, borrow=False)
    
    # for momentum
    old_update = theano.shared(np.zeros(w.get_value(borrow=True).shape),
                               borrow=True)
    
    # 1つのデータ(60, 2) の代わり
    x_sy = T.matrix("x")
    # １つのtarget (2, )の代わり
    t_sy = T.vector("y")
    
    # ６０この回転伸縮行列
    w_mat_list = []
    for i in range(n_tensor):
        r = T.as_tensor([w[2*i], -w[2*i+1], w[2*i+1], w[2*i]])
        r = r.reshape((2, 2))
        w_mat_list.append(r)
        
    h = T.sum([T.dot(w_mat_list[i], x_sy[i]) for i in xrange(n_tensor)], axis=0)

    # L2_cost
    cost = T.sum((h - t_sy)**2)
    obj = cost + s_reg2*w.norm(L=2) + s_reg1*w.norm(L=1)
    # momentum
    grads = T.grad(cost=obj, wrt=w)
    delta = -s_lr * grads + s_mom * old_update
    
    updates = [(w, w + delta),
               (old_update, delta)]
    
    index = T.iscalar("index")
    
    f_train  = theano.function(
        inputs=[index],
        outputs=[cost],
        updates=updates,
        givens=[(x_sy, s_input[index]),
                (t_sy, s_target[index])]
        )
        
    f_decay_lr = theano.function(
        inputs=[],
        updates=[(s_lr, s_lr*0.98)]
        )
        
    f_update_reg = theano.function(
        inputs=[],
        updates=[(s_reg2, reg2),
                 (s_reg1, reg1)]
        )
        
    start_time = time.clock()
    cost_array = np.zeros((n_epochs,))
    
    print "training model ....!"
    
    for i_epoch in xrange(n_epochs):
        f_decay_lr()
        if i_epoch == 1:
            f_update_reg()
        for i_pix in xrange(featureImg.shape[0]):
            cost_i_pix = f_train(i_pix)
            cost_array[i_epoch] = cost_array[i_epoch] + cost_i_pix
        print i_epoch, cost_array[i_epoch]
        
    print "Training took", time.clock() - start_time, "sec"
    
    w = w.get_value()
    tmp = np.zeros(w.shape)
    tmp[::2] = norm_coef
    tmp[1::2] = norm_coef
    
    w = w / tmp
        
    return w, cost_array
    
    
                                        
