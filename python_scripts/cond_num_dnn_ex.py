# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 20:33:26 2016

@author: matsuik
"""
import numpy as np
import TN_utils

def cond_num_dnn_ex(vnn, param_list, feature_list, GT_list,
                    lr_list, epoch_list, featureImg_list):
    
    batch_size = 100
    l2 = 0.
    
    train_error_list = []
    test_error_list = []
    cn11236 = []
    det11236 = []
    wsc11236 = []
    gt1img360_list = []
    gt1img120_list = []    
    
    for i in range(3):
        vnn.initialize_params(param_list)
        vnn.set_training(feature_list[i], GT_list[i])
        
        n_batchs = vnn.n_pixels // batch_size
        
        n_epochs = epoch_list[i]
        interval = n_epochs // 15
        training_MSE_array = np.zeros((n_epochs // interval))
        test_MSE_array = np.zeros((n_epochs // interval))
        
        cn_list_list = []
        det_list_list = []
        weighted_sum_list = []
        lr = lr_list[i]
        for i_epoch in xrange(n_epochs):
            for i_batch in xrange(n_batchs):
                vnn.train(i_batch, lr, l2, batch_size)
                
            if i_epoch % interval == 0:
                MSE = vnn.training_error()
                training_MSE_array[i_epoch // interval] = MSE
                MSE = vnn.test_error()
                test_MSE_array[i_epoch // interval] = MSE
                
                w_list = vnn.get_w_list()
                cn_list = []
                det_list = []
                for aw in w_list:
                    cn_list.append(TN_utils.clcl_cn_inv(aw))
                    det_list.append(np.linalg.det(aw))
                cn_list_list.append(cn_list)
                det_list_list.append(det_list)
                w_norm_list = np.asarray([np.sum(w**2) for w in w_list])
                weighted_sum = np.sum(np.asarray(cn_list) * np.asarray(w_norm_list))
                n_weighted_sum = weighted_sum / np.sum(w_norm_list)
                weighted_sum_list.append(n_weighted_sum)
                
                print i_epoch, MSE
                
        gt2vec = vnn.output(featureImg_list[0])
        gt1img360 = TN_utils.convert_gt2vec_to_gt1img(gt2vec, img_shape=(256, 256))
        
        
        gt2vec = vnn.output(featureImg_list[11])
        gt1img120 = TN_utils.convert_gt2vec_to_gt1img(gt2vec, img_shape=(256, 256))
        
        train_error_list.append(training_MSE_array)
        test_error_list.append(test_MSE_array)
        cn11236.append(cn_list_list)
        det11236.append(det_list_list)
        wsc11236.append(weighted_sum_list)
        gt1img360_list.append(gt1img360)
        gt1img120_list.append(gt1img120)
        
    results = [train_error_list, test_error_list, cn11236, det11236, wsc11236, gt1img360_list, gt1img120_list]
    return results