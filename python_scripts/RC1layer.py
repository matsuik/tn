# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 16:33:48 2016

@author: matsuik
"""
from __future__ import division

import numpy as np

import theano
import theano.tensor as T

def ab(featureImg, imgGT, target_featureImg, target_imgGT,
       out_activation):
    """
    Parameters
    -----------
    
    featureImg : ndarray
        (n_pixels, n_tensor, 2)
    imgGT : ndarray
        (n_pixels, 2)
    target_featureImg, target_imgGT
    out_activation : str
        "sigm" , "ReLU" or "linear"
    """
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True)
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "gt2", borrow=True)
                             
    s_test_input = theano.shared(np.asarray(target_featureImg, dtype=theano.config.floatX),
                            "target_featureImg", borrow=True)
    s_test_target = theano.shared(np.asarray(target_imgGT, dtype=theano.config.floatX),
                             "target_imgGT", borrow=True)
    
    
    s_reg2 = T.scalar()
    
    s_lr = T.scalar()
    
    batch_size = T.iscalar()
    
    # 1つのデータ (batch, 60, 2)の代わり
    x_sy = T.tensor3("x")
    # １つのtarget (batch, 2)の代わり
    t_sy = T.matrix("y")
    
### 2段目
    # 60個のa2
    a2 = theano.shared(np.asarray(0.01*np.random.normal(size=(n_tensor, )),
                                  dtype=theano.config.floatX))
    # 60個のb2
    b2 = theano.shared(np.asarray(0.01*np.random.normal(size=(n_tensor, )),
                                  dtype=theano.config.floatX))   

    # ６０この回転伸縮行列
    orth = T.as_tensor([a2, -b2, b2, a2]) # (4, n_hidden)
    orth_t = orth.T # (n_hidden, 4)
    w_mats = orth_t.reshape((n_tensor, 2, 2))
        
    # (batch, 2) <- (batch_size, n_hidden, 2) * (n_hidden, 2, 2)
    if out_activation == "linear":
        param2_list = [a2, b2]
        out = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
    if out_activation == "ReLU":
        bias2 = theano.shared(0.)
        param2_list = [a2, b2, bias2]
        h = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
        def ReLU(x):
            return T.switch(x<0, 0, x)
        out = ReLU(h.norm(L=2, axis=1) + bias2).dimshuffle(0, "x") * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    if out_activation == "sigmoid":
        bias2 = theano.shared(0.)
        param2_list = [a2, b2, bias2]
        h = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
        out = T.nnet.sigmoid(h.norm(L=2, axis=1) + bias2).dimshuffle(0, "x") * h / (np.finfo(np.float64).eps + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    
#　costの定義と最適化

    cost = T.mean((out - t_sy)**2)
    obj = cost + s_reg2*(a2.norm(L=2) + b2.norm(L=2))
    
    grad2_list = [T.grad(cost=obj, wrt=param) for param in param2_list]
    
    #delta_list = [-s_lr / batch_size * grads for grads in grad_list]
    delta2_list = [-s_lr/batch_size * grads for grads in grad2_list]
    
    updated2_list = [param + delta for param, delta in zip(param2_list, delta2_list)]
    
    updates = zip(param2_list, updated2_list)
    
    
    index = T.iscalar("index")
    
    
    f_train = theano.function(
        inputs=[index, s_lr, s_reg2, batch_size],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])]
        )
    
    f_training_error = theano.function(
        inputs=[],
        outputs=[cost],
        givens=[(x_sy, s_input),
                (t_sy, s_target)]
        )
        
    f_test_error = theano.function(
        inputs=[],
        outputs=[cost],
        givens=[(x_sy, s_test_input),
                (t_sy, s_test_target)]
        )
    
    f_output = theano.function(
        inputs=[x_sy],
        outputs=[out]
        )
        
    result = [f_train, f_training_error, f_test_error, f_output, s_input, s_target, param2_list]
        
    return result