# -*- coding: utf-8 -*-
"""
Created on Tue Dec 29 20:52:33 2015

@author: matsuik
"""

from __future__ import division

import time
import numpy as np
from scipy.linalg import svdvals

import theano_dnn

def cn_experiment(data_dict, epoch_dict):
    start_time = time.clock()
    batch_size = 100
    lr = 0.001
    l2 = 0.
    cn = {}
    activation_list = ["ReLU", "ReLU"]
    for n_hidden in [20]:
        for n_samples in ['t1', 't12', 't36']:
            feature = data_dict["feature"+n_samples]
            GT = data_dict["GT"+n_samples]
            
            norm_coef = np.std(np.linalg.norm(feature, axis=2), axis=0)
            feature = feature / norm_coef[:, np.newaxis]
            
            n_epochs = epoch_dict[n_samples]
            n_batchs = feature.shape[0] // batch_size
            feature = feature.reshape(feature.shape[0], 120)
            
            cn[n_samples+str(n_hidden)] = []
            
            for i_train in range(10):
                
                param_list = [0.01 * np.random.normal(size=(120, n_hidden)),
                              np.zeros(n_hidden),
                                0.01 * np.random.normal(size=(n_hidden, 2)),
                                np.zeros(2)]
                train_model, get_test_error, f_out, dnn = theano_dnn.optimize_graph(
                        feature, GT, feature, GT,
                        param_list, ["w1", "b1", "w2", "b2"], activation_list,
                        drop_p=1., input_drop=1., vector_activation_shape_list=[2, 2])
                for i_epoch in xrange(n_epochs):
                    for i_batch in xrange(n_batchs):
                        train_model(i_batch, lr, l2, batch_size)
                
                w1 = dnn.hidden_layers[0].W.get_value()
                w2 = dnn.output_layer.W.get_value()
                
                w_list = []
                for i in range(60):
                    for j in range(n_hidden//2):
                        w_list.append(w1[2*i:2*i+2, 2*j:2*j+2])
                for i in range(n_hidden//2):
                    w_list.append(w2[2*i:2*i+2])
                
                cn_list = []
                for aw in w_list:
                    sv = svdvals(aw)
                    cn_list.append(sv[1] / sv[0])
                    
                cn[n_samples+str(n_hidden)].extend(cn_list)
                
                print n_hidden, n_samples, i_train, (time.clock() - start_time) // 1
    return cn
                