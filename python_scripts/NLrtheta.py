# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 15:56:00 2015

@author: matsuik
"""

from __future__ import division

import numpy as np

import theano
import theano.tensor as T

def optimize_graph(featureImg, imgGT, batch_size):
    """
    featureImg (n_pixels, n_tensor, 2)
    imgGT (n_pixels, 2)
    """
    
    # データの正規化
    norm_coef = np.std(np.linalg.norm(featureImg, axis=2), axis=0) # (60,)
    featureImg = featureImg / norm_coef[:, np.newaxis]
    
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True,
                            )
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "gt2", borrow=True,
                             )
                             
    s_reg2 = T.scalar()
    s_reg1 = T.scalar()
    
    s_lr = T.scalar()
    s_mom = T.scalar()
    
    # 60個のr
    magni = theano.shared(np.random.uniform(low=-0.01, high=0.01,
                                        size=(n_tensor, )))
    # 60個のtheta
    theta = theano.shared(np.random.uniform(low=-np.pi, high=np.pi,
                                        size=(n_tensor, )))

        
    bias = theano.shared(0.)
    
    old_updates = [theano.shared(np.zeros(n_tensor), borrow=True),
                   theano.shared(np.zeros(n_tensor), borrow=True),
                    theano.shared(0.)]

    
    
    # 1つのデータ (batch, 60, 2)の代わり
    x_sy = T.tensor3("x")
    # １つのtarget (batch, 2)の代わり
    t_sy = T.matrix("y")
    
    # ６０この回転伸縮行列
    cos = T.cos(theta)
    sin = T.sin(theta)
    rot = T.as_tensor([cos, -sin, sin, cos]) # (4, n_tensor)
    orth = magni * rot # (4, n_tensor)
    orth_t = orth.T # (n_tensor, 4)
    w_mats = orth_t.reshape((n_tensor, 2, 2))
    
    
    # (batch, 2) <- (batch_size, n_tensor, 2) * (n_tensor, 2, 2)
    h = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
    

    out = T.nnet.sigmoid(h.norm(L=2, axis=1) + bias).dimshuffle(0, "x") * h / (h.norm(L=2, axis=1).dimshuffle(0, "x"))

    cost = T.sum((out - t_sy)**2)
    obj = cost + s_reg2*magni.norm(L=2) + s_reg1*magni.norm(L=1)
    
    grad_list = [T.grad(cost=obj, wrt=param) for param in (magni, theta, bias)]
    #delta_list = [-s_lr / batch_size * grads for grads in grad_list]
    delta_list = [-s_lr/batch_size * grads + s_mom * old_u for grads, old_u in zip(grad_list, old_updates)]

    
    updates = [(magni, magni + delta_list[0]),
               (theta, theta + delta_list[1]),
                (bias, bias + delta_list[2])] + zip(old_updates, delta_list)
    
    index = T.iscalar("index")
    
    f_train = theano.function(
        inputs=[index, s_lr, s_mom, s_reg2, s_reg1],
        outputs=[cost],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])],
        profile=True
        )
#    
#    f_train_magni  = theano.function(
#        inputs=[index, s_lr, s_mom, s_reg2, s_reg1],
#        outputs=[cost],
#        updates=[updates[0], updates[2]],
#        givens=[(x_sy, s_input[index : index+batch_size]),
#                (t_sy, s_target[index : index+batch_size])]
#        )
#    
#    f_train_theta  = theano.function(
#        inputs=[index, s_lr, s_mom, s_reg2, s_reg1],
#        outputs=[cost],
#        updates=updates[1:],
#        givens=[(x_sy, s_input[index : index+batch_size]),
#                (t_sy, s_target[index : index+batch_size])]
#        )
#        
    f_output = theano.function(
        inputs=[x_sy],
        outputs=[out],
        profile=True
        )

        
    return f_train, f_output, s_input, s_target, magni, theta, bias, old_updates, norm_coef
    
    
                                        
