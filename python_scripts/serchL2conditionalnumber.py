# -*- coding: utf-8 -*-
"""
Created on Sun Jan 10 15:55:38 2016

@author: matsuik
"""
from __future__ import division


import numpy as np

import matplotlib.pyplot as plt

import theano_dnn

from scipy.linalg import svdvals

plt.set_cmap("gray")
plt.rcParams["image.interpolation"] = None


def serchL2(input, target, test_data, norm_coef, n_hidden, batch_size, lr, l2_list, n_epochs, test_interval):
    MSE_array_list = []
    gt1_comp_list = []
    cn_list_list = []
    for l2 in l2_list:
        activation_list = ["ReLU", "ReLU"]
        param_list = [0.01 * np.random.normal(size=(120, 20)), np.zeros(20), 0.01 * np.random.normal(size=(20, 2)), np.zeros(2)]
        
        train_model, get_test_error, f_out, dnn = theano_dnn.optimize_graph(input, target, input, target,
            param_list, ["w1", "b1", "w2", "b2"], activation_list,
            drop_p=1., input_drop=1., vector_activation_shape_list=[2, 2])
            
        n_batchs = input.shape[0] // batch_size
        
        MSE_array = np.zeros(n_epochs // test_interval)
        for i_epoch in xrange(n_epochs):
            for i_batch in xrange(n_batchs):
                train_model(i_batch, lr, l2, batch_size)
        
            if i_epoch % test_interval == 0:
                MSE = get_test_error()
                MSE_array[i_epoch // test_interval] = MSE
                print i_epoch, MSE
        
        plt.figure()
        plt.plot(MSE_array)
        MSE_array_list.append(MSE_array)
        
        a = f_out((test_data/norm_coef[:, np.newaxis]).reshape(256*256, 120))
        gt2_v =  a
        gt2_comp = gt2_v[:, 0] + 1j*gt2_v[:, 1]
        norm = np.absolute(gt2_comp)
        gt1_comp = norm * np.sqrt(gt2_comp / (norm+np.finfo(np.float64).eps))
        
        plt.figure(figsize=(5, 5))
        plt.imshow(np.absolute(gt1_comp).reshape((256, 256)))
        gt1_comp_list.append(gt1_comp)
        
        w = dnn.hidden_layers[0].W.get_value()
        
        w_list = []
        for i in range(60):
            for j in range(n_hidden // 2):
                w_list.append(w[2*i:2*i+2, 2*j:2*j+2])
        
        cn_list = []
        for aw in w_list:
            sv = svdvals(aw)
            cn_list.append(sv[1] / sv[0])
            
        gomi = plt.hist([cn_list], bins=10, alpha=0.8)
        cn_list_list.append(cn_list)
        
    return MSE_array_list, cn_list_list