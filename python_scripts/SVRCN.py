# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 12:57:30 2015

@author: matsuik
"""

from __future__ import division

import numpy as np

import theano
import theano.tensor as T



def optimize_graph(featureImg, der, imgGT, n_hidden, activation):
    """
    featureImg (n_pixels, n_tensor, 2)
    imgGT (n_pixels, 2)
    """
    
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True)
    s_der = theano.shared(np.asarray(der, dtype=theano.config.floatX),
                            "der", borrow=True)
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "gt2", borrow=True)
    
    s_reg2 = T.scalar()
    s_reg1 = T.scalar()
    
    s_lr = T.scalar()
    
    batch_size = T.iscalar()
    
    hidden_list = []
    magni_list = []
    theta_list = []
    
    # featureImg(batch, 60, 2)の代わり
    x_sy = T.tensor3("x")
    
    # der (batch, 25)の代わり
    der_sy = T.matrix("der_sy")    
    # １つのtarget (batch, 2)の代わり
    t_sy = T.matrix("y")
    
    if activation == "sigmoid":
        activate = T.nnet.sigmoid
    elif activation == "ReLU":
        def activate(x):
            return T.switch(x<0, 0, x)

    
### 1段目    
    for i_hidden in range(n_hidden):
        # 60個のr
        magni = theano.shared(np.random.uniform(low=-0.01, high=0.01,
                                            size=(n_tensor, )))
        # 60個のtheta
        theta = theano.shared(np.random.uniform(low=-np.pi, high=np.pi,
                                            size=(n_tensor, )))
        
        # ６０この回転伸縮行列
        cos = T.cos(theta)
        sin = T.sin(theta)
        rot = T.as_tensor([cos, -sin, sin, cos]) # (4, n_tensor)
        orth = magni * rot # (4, n_tensor)
        orth_t = orth.T # (n_tensor, 4)
        w_mats = orth_t.reshape((n_tensor, 2, 2))
        
        
        # (batch, 2) <- (batch_size, n_tensor, 2) * (n_tensor, 2, 2)
        h = T.tensordot(x_sy, w_mats, axes=[[1, 2], [0, 2]])
        
        hidden_list.append(h) # (n_hidden, batch, 2)
        magni_list.append(magni)
        theta_list.append(theta)
    
    # 2nd order tensorを足し合わせたもの
    hidden_vec = T.stacklists(hidden_list).dimshuffle(1, 0, 2) # (batch, n_hidden, 2)
    
    # derをsuperpositionするweight matrix
    W = theano.shared(0.01 * np.random.normal(size=(25, n_hidden)))
    bias = theano.shared(np.zeros(n_hidden))
    
    # norm_derを足したもの(batch, n_hidden)
    hidden_norm = T.dot(der_sy, W)
    
    act = hidden_norm.dimshuffle(0, 1, "x") * hidden_vec # (batch, n_hidden, 2)
    act_norm = act.norm(L=2, axis=2).dimshuffle(0, 1, "x") # (batch, n_hidden, "x")
    hiddens = activate(act_norm + bias.dimshuffle(0, "x")) * act / (act_norm + 10**(-9))
    
### 2段目
    # 60個のr
    magni2 = theano.shared(np.random.uniform(low=-0.01, high=0.01,
                                        size=(n_hidden, )))
    # 60個のtheta
    theta2 = theano.shared(np.random.uniform(low=-np.pi, high=np.pi,
                                        size=(n_hidden, )))                                                

    # ６０この回転伸縮行列
    cos = T.cos(theta2)
    sin = T.sin(theta2)
    rot = T.as_tensor([cos, -sin, sin, cos]) # (4, n_hidden)
    orth = magni2 * rot # (4, n_hidden)
    orth_t = orth.T # (n_hidden, 4)
    w_mats = orth_t.reshape((n_hidden, 2, 2))
        
    # (batch, 2) <- (batch_size, n_hidden, 2) * (n_hidden, 2, 2)
    out = T.tensordot(hiddens, w_mats, axes=[[1, 2], [0, 2]])
    
#　costの定義と最適化

    cost = T.sum((out - t_sy)**2)
    magniL2 = T.sum([m.norm(L=2) for m in magni_list])
    magniL1 = T.sum([m.norm(L=1) for m in magni_list])
    obj = cost + s_reg2*(magni2.norm(L=2) + magniL2 + W.norm(L=2)) + s_reg1*(magni2.norm(L=1) + magniL1)
    
    
    param2_list = [magni2, theta2, W, bias]
    
    grad2_list = [T.grad(cost=obj, wrt=param) for param in param2_list]
    magni_grad_list = [T.grad(cost=obj, wrt=m) for m in magni_list]
    theta_grad_list = [T.grad(cost=obj, wrt=t) for t in theta_list]
    
    #delta_list = [-s_lr / batch_size * grads for grads in grad_list]
    delta2_list = [-s_lr/batch_size * grads for grads in grad2_list]
    magni_delta_list = [-s_lr/batch_size * grads for grads in magni_grad_list]
    theta_delta_list = [-s_lr/batch_size * grads for grads in theta_grad_list]
    
    updated2_list = [param + delta for param, delta in zip(param2_list, delta2_list)]
    magni_updated_list = [param + delta for param, delta in zip(magni_list, magni_delta_list)]
    theta_updated_list = [param + delta for param, delta in zip(theta_list, theta_delta_list)]

    updates = zip(param2_list, updated2_list) + zip(magni_list, magni_updated_list) + \
        zip(theta_list, theta_updated_list)
    
    
    index = T.iscalar("index")
    
    
    f_train = theano.function(
        inputs=[index, s_lr, s_reg2, s_reg1, batch_size],
        outputs=[cost],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (der_sy, s_der[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])],
        profile=True
        )
        
    f_output = theano.function(
        inputs=[x_sy, der_sy],
        outputs=[out],
        profile=True
        )
        
    return f_train, f_output, s_input, s_der, s_target, param2_list, magni_list, theta_list