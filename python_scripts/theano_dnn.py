# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 21:05:08 2015

@author: matsuik
"""
from __future__ import division

import time

from numpy import *
import numpy as np
import matplotlib.pyplot as plt
from imtools import set_imshow
set_imshow()

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

import TN_utils

def make_shared(x, name, dtype=None):
    if dtype == None:
        dtype = theano.config.floatX
    
    shared_var = theano.shared(value=array(x, dtype=dtype),
                               name=name,
                               borrow=False)
    return shared_var


def init_params(dim_list, activation_list):
    param_list = []
    for i in range(len(dim_list)-1):
        if activation_list[i] == "sigm":
            dim1 = dim_list[i]
            dim2 = dim_list[i+1]
            W = np.random.uniform(low=-np.sqrt(6. / (dim1 + dim2)),
                    high=np.sqrt(6. / (dim1 + dim2)),
                    size=(dim1, dim2)
                    )
        
        if activation_list[i] == "tanh":
            dim1 = dim_list[i]
            dim2 = dim_list[i+1]
            W = np.random.uniform(low=-np.sqrt(4. / (dim1 + dim2)),
                    high=np.sqrt(4. / (dim1 + dim2)),
                    size=(dim1, dim2)
                    )
        
        elif activation_list[i] == "ReLU":
            dim1 = dim_list[i]
            dim2 = dim_list[i+1]
            W = 0.01*np.random.randn(dim1, dim2)
        
        elif activation_list[i] == "softmax":
            W = np.zeros((dim_list[i], dim_list[i+1]))
            
        param_list.append(W)
        param_list.append(np.zeros((dim_list[i+1], )))
    return param_list

def get_activate_func(act_name, vector_activation_shape):
    """
    Parameters
    ----------
    act_name : str
        "linear", "sigm", "tanh", "ReLU", or "softmax"
    vector_activation_shape : int or False
    
    Returns
    -------
    theano.tensor.elemwise
    """
    
    if act_name == "linear":
        def linear(x):
            return x
        activ_func = linear
        
    elif act_name == "sigm":
        activ_func= T.nnet.sigmoid
    
    elif act_name == "tanh":
        activ_func = T.tanh
        
    elif act_name == "ReLU":
        def relu(x):
            return T.switch(x<0, 0, x)
        activ_func = relu
        
    elif act_name == "Resigm":
        def relu(x):
            return T.switch(x<0, 0, T.nnet.sigmoid(x))
        activ_func = relu
        
    elif activation == "softmax":
        activ_func = T.nnet.softmax
        
    if vector_activation_shape:
        def vector_activation(x, bias):
            shaped_x = x.reshape((x.shape[0]*x.shape[1]//vector_activation_shape,
                                  vector_activation_shape))
            norm = shaped_x.norm(L=2, axis=1).dimshuffle(0, "x")
            activated_x = activ_func(norm + T.extra_ops.repeat(bias, x.shape[0]).dimshuffle(0, "x")) * shaped_x / norm
            return activated_x.reshape(x.shape)
        return vector_activation
    else:
        return activ_func



class OutputLayer(object):
    def __init__(self, 
                 input_learn,
                 input_predict,
                 target,
                 W,
                 bias,
                 act_name,
                 theano_rng,
                 vector_activation_shape
                 ):
                     
        """
            input_learn, input_predict (N, in_dim), target (N, out_dim), W (in_dim, out_dim), bias (out_dim): 
                type: TensorVariable
            if activation == "softmax", target
            activation: type: str
                "sigm", "linear" of "softmax"
        """
        
        self.input_learn = input_learn
        self.input_predict = input_predict
        self.target = target
        
        self.theano_rng = theano_rng
        
        self.activation = get_activate_func(act_name, vector_activation_shape)
        
        if act_name in ["linear", "sigm", "tanh", "ReLU", "Resigm"]:
            self.output_error = self.MSE
        else:
            self.output_error = self.cross_entropy
    
            
        self.W = W
        self.b = bias
        
        self.params = [self.W, self.b]
    

    def forward_prop_learn(self):
        
        return self.activation(T.dot(self.input_learn, self.W) + self.b)
    
    
    def forward_prop_predict(self):
        return self.activation(T.dot(self.input_predict, self.W) + self.b)
    
    
    def MSE(self):
        output = self.forward_prop_learn()
        return T.mean((self.target - output)**2)
        
    def test_MSE(self):
        output = self.forward_prop_predict()
        return T.mean((self.target - output)**2)
    
    
    def cross_entropy(self):
        output = self.forward_prop_learn()
        
        # negative log likelihookを最小化する
        # a[行に対するインデックスのリスト, 列に対するインデックスのリスト]
        # where len(行に対するインデックスのリスト) == len(列に対するインデックスのリスト)
        NLL = -T.log(output)[T.arange(output.shape[0]), self.target]
        return T.mean(NLL)

   
    def get_accuracy(self):
        output = self.forward_prop_predict()
        return T.mean(T.eq(T.argmax(self.target, axis=1), T.argmax(output, axis=1)))
        

class VectorOutputLayer(object):
    def __init__(self, 
                 input_learn,
                 input_predict,
                 target,
                 W,
                 bias,
                 act_name,
                 theano_rng,
                 vector_activation_shape
                 ):
                     
        """
            input_learn, input_predict (N, in_dim), target (N, out_dim), W (in_dim, out_dim), bias (n_vector_units,): 
                type: TensorVariable
            if activation == "softmax", target
            activation: type: str
                "sigm", "linear" of "softmax"
        """
        
        self.input_learn = input_learn
        self.input_predict = input_predict
        self.target = target
        
        self.theano_rng = theano_rng
        
        self.activation = get_activate_func(act_name, vector_activation_shape)
        
        if act_name in ["linear", "sigm", "tanh", "ReLU", "Resigm"]:
            self.output_error = self.MSE
        else:
            self.output_error = self.cross_entropy
    
            
        self.W = W
        self.b = bias
        
        self.params = [self.W, self.b]
    

    def forward_prop_learn(self):
        
        return self.activation(T.dot(self.input_learn, self.W), self.b)
    
    
    def forward_prop_predict(self):
        return self.activation(T.dot(self.input_predict, self.W), self.b)
    
    
    def MSE(self):
        output = self.forward_prop_learn()
        return T.mean((self.target - output)**2)
        
    def test_MSE(self):
        output = self.forward_prop_predict()
        return T.mean((self.target - output)**2)
    
    
    def cross_entropy(self):
        output = self.forward_prop_learn()
        
        # negative log likelihookを最小化する
        # a[行に対するインデックスのリスト, 列に対するインデックスのリスト]
        # where len(行に対するインデックスのリスト) == len(列に対するインデックスのリスト)
        NLL = -T.log(output)[T.arange(output.shape[0]), self.target]
        return T.mean(NLL)

   
    def get_accuracy(self):
        output = self.forward_prop_predict()
        return T.mean(T.eq(T.argmax(self.target, axis=1), T.argmax(output, axis=1)))
        
        


class HiddenLayer(object):
    def __init__(self,
                 input_learn,
                 input_predict,
                 W,
                 bias,
                 act_name,
                 drop_p,
                 theano_rng,
                 vector_activation_shape
                 ):
        """
            input (N, in_dim), W (in_dim, out_dim), bias (out_dim): 
                type: TensorVariable
            activation: type: str
                "sigm" or "linear"
        """
        
        self.input_learn = input_learn
        self.input_predict = input_predict
        self.W = W
        self.b = bias
        self.drop_p = drop_p
        
        self.theano_rng = theano_rng
        
        self.activation = get_activate_func(act_name, vector_activation_shape)
            
        self.params = [self.W, self.b]
    
    
    def forward_prop_learn(self):
        dropout_mask = self.theano_rng.binomial(size=self.input_learn.shape,
                                           p=self.drop_p,
                                           dtype=theano.config.floatX)
        return self.activation(T.dot(self.input_learn*dropout_mask, self.W) + self.b)
        
    
    def forward_prop_predict(self):
        return self.activation(T.dot(self.input_predict*self.drop_p, self.W) + self.b)
        
        
class VectorHiddenLayer(object):
    def __init__(self,
                 input_learn,
                 input_predict,
                 W,
                 bias,
                 act_name,
                 drop_p,
                 theano_rng,
                 vector_activation_shape
                 ):
        """
            input (N, in_dim), W (in_dim, out_dim), bias (n_vector_units,): 
                type: TensorVariable
            activation: type: str
                "sigm" or "linear"
        """
        
        self.input_learn = input_learn
        self.input_predict = input_predict
        self.W = W
        self.b = bias
        self.drop_p = drop_p
        
        self.theano_rng = theano_rng
        
        self.activation = get_activate_func(act_name, vector_activation_shape)
            
        self.params = [self.W, self.b]
    
    
    def forward_prop_learn(self):
        dropout_mask = self.theano_rng.binomial(size=self.input_learn.shape,
                                           p=self.drop_p,
                                           dtype=theano.config.floatX)
        return self.activation(T.dot(self.input_learn*dropout_mask, self.W), self.b)
        
    
    def forward_prop_predict(self):
        return self.activation(T.dot(self.input_predict*self.drop_p, self.W), self.b)
    

class DNN(object):
    def __init__(self,
                 input,
                 target,
                 param_list,
                 activation_list,
                 drop_p,
                 input_drop,
                 theano_rng,
                 vector_activation_shape_list
                 ):
        """
            input (N, in_dim), target(N, out_dim): type: TensorSharedVariable
            param_list: list of TensorSharedVariable
                [W_bottom, bias_bottom, W_2, bias_2, ..., W_top, bias_top]
                length is 2*(number of perceptrons)
                W (in_dim, out_dim): TensorVariable
                b (out_dim, ) : TensorVariable
            activation_list: list of str
                ["sigm", ..., "linear"]
                "softmax" for output_layer
            drop_p, input_drop : float
            vector_activation_shape_list : list of int
                [dim, dim]
        """
        
        hidden_layers = []
        
        # input layer
        layer = VectorHiddenLayer(input_learn=input, input_predict=input,
                            W=param_list[0], bias=param_list[1],
                            act_name=activation_list[0],
                            drop_p=input_drop, theano_rng=theano_rng,
                            vector_activation_shape=vector_activation_shape_list[0]
                            )
        hidden_layers.append(layer)
        
        # internal layer
        for i in range(len(param_list)//2)[1:-1]:
            layer = VectorHiddenLayer(input_learn=hidden_layers[i-1].forward_prop_learn(),
                                input_predict=hidden_layers[i-1].forward_prop_predict(),
                                W=param_list[2*i],
                                bias=param_list[2*i+1],
                                act_name=activation_list[i],
                                drop_p=drop_p, theano_rng=theano_rng,
                                vector_activation_shape=vector_activation_shape_list[i]
                                )
            hidden_layers.append(layer)
            
        self.hidden_layers = hidden_layers
            
        # output layer
        self.output_layer = VectorOutputLayer(input_learn=hidden_layers[-1].forward_prop_learn(),
                                        input_predict=hidden_layers[-1].forward_prop_predict(),
                                        target=target,
                                        W=param_list[-2],
                                        bias=param_list[-1],
                                        act_name=activation_list[-1],
                                        theano_rng=theano_rng,
                                        vector_activation_shape=vector_activation_shape_list[-1]
                                        )
        
        params = []
        for layer in hidden_layers:
            params = params + layer.params
        self.params = params + self.output_layer.params
        
        self.n_layers = len(activation_list)
     
     
    def get_update_list(self, lr, L2_lambda):
        """
            return: updates: list
                [(param1, how to update it), (param2, how..), ...]
        """
        
        cost = self.cost(L2_lambda)
        
        lr_list = []
        for i in range(self.n_layers)[::1]:
            layer_lr = 1.2**i * lr 
            lr_list.append(layer_lr)
            lr_list.append(layer_lr)
            
        grads = [T.grad(cost, wrt=parameter) for parameter in self.params]
        updates = [(param, param - alr*grad) for param, grad, alr
            in zip(self.params, grads, lr_list)]
        return updates
    
    
    def cost(self, L2_lambda): 
        output_error = self.output_layer.output_error()
        
        L2reg = T.sum(self.output_layer.W ** 2)
        for layer in self.hidden_layers:
            L2reg = L2reg + T.sum(layer.W ** 2)
        
        
        return output_error + L2_lambda * L2reg
        
        
    def output(self):
        return self.output_layer.forward_prop_predict()
        
        
class VLR(object):
    def __init__(self,
                 input,
                 target,
                 param_list,
                 activation_list,
                 drop_p,
                 input_drop,
                 theano_rng,
                 vector_activation_shape_list
                 ):
        """
        Parameters
        ----------
        
        input (N, in_dim), target(N, out_dim): type: TensorSharedVariable
        param_list: list of theano.TensorSharedVariable
            [W, b]
            W (in_dim, out_dim): TensorVariable
            b (out_dim, ) : TensorVariable
        activation_list: list of str
            ["ReLU"]
        drop_p, input_drop : int
        vector_activation_list : list of int
            [dim]
        """
        
        # output layer
        self.output_layer = VectorOutputLayer(input_learn=input,
                                        input_predict=input,
                                        target=target,
                                        W=param_list[0],
                                        bias=param_list[1],
                                        act_name=activation_list[0],
                                        theano_rng=theano_rng,
                                        vector_activation_shape=vector_activation_shape_list[0]
                                        )
        
        self.params =  self.output_layer.params
        
        self.n_layers = len(activation_list)
     
     
    def get_update_list(self, lr, L2_lambda):
        """
            return: updates: list
                [(param1, how to update it), (param2, how..), ...]
        """
        
        cost = self.cost(L2_lambda)
            
        grads = [T.grad(cost, wrt=parameter) for parameter in self.params]
        updates = [(param, param - lr*grad) for param, grad
            in zip(self.params, grads)]
        return updates
    
    
    def cost(self, L2_lambda): 
        output_error = self.output_layer.output_error()
        
        L2reg = T.sum(self.output_layer.W ** 2)
        return output_error + L2_lambda * L2reg
        
        
    def output(self):
        return self.output_layer.forward_prop_predict()


def optimize_graph(data, target, test_data, test_target,
      param_list, param_name_list, activation_list,
      drop_p, input_drop,
      vector_activation_shape_list,
      n_layers):
    """
    
    Parameters
    ----------
    data, target: type: ndarray
    param_list: list of ndarray
            [W_bottom, bias_bottom, W_2, bias_2, ..., W_top, bias_top]
            length // 2 is number of perceptrons
            W (in_dim, out_dim): ndarray
            b (out_dim, ) : ndarray
    drop_p, input_drop : float
    param_name_list: list of string
    activation_list: list:
            ["sigm", ..., "linear"]
    vector_activation_shape_list : list of int
        [dim, ..., dim]
        if you do not use vector activation, give [False, False,..]
        that has the same shape as activation_list
    n_layers : int
    """
    
    theano_rng = RandomStreams(1999)    

    
    s_data = make_shared(data, "data")
    s_test_data = make_shared(test_data, "test_data")

    s_target = make_shared(target, "target")
    s_test_target = make_shared(test_target, "test_target")
    
    s_param_list = [make_shared(param, name) for param, name 
        in zip(param_list, param_name_list)]
    
    s_lr = T.scalar("lr")
    L2_lambda = T.scalar("l2")
    batch_size = T.iscalar("batch_size")
    # minibatchに依存するシンボルは、仮置きしといてtheano.function(givens=)で
    # TensorSharedVariableを渡す。
    x = T.matrix("x")
    y = T.matrix("y")
    
    if n_layers==2:
        dnn = DNN(input=x, target=y, param_list=s_param_list, activation_list=activation_list,
              drop_p=drop_p, input_drop=input_drop, theano_rng=theano_rng,
              vector_activation_shape_list=vector_activation_shape_list)
    if n_layers==1:
        dnn = VLR(input=x, target=y, param_list=s_param_list, activation_list=activation_list,
              drop_p=drop_p, input_drop=input_drop, theano_rng=theano_rng,
              vector_activation_shape_list=vector_activation_shape_list)
    index = T.iscalar("index")
    
    f_train = theano.function(
        inputs=[index, s_lr, L2_lambda, batch_size],
        updates=dnn.get_update_list(s_lr, L2_lambda),
        givens=[(x, s_data[index*batch_size : (index+1)*batch_size]),
                (y, s_target[index*batch_size : (index+1)*batch_size])]
        )
    
    f_training_error = theano.function(
        inputs=[],
        outputs=dnn.output_layer.output_error(),
        givens=[(x, s_data),
                (y, s_target)]
        )
    
     
    f_test_error = theano.function(
        inputs=[],
        outputs=dnn.output_layer.test_MSE(),
        givens=[(x, s_test_data),
                (y, s_test_target)]
        )
        
    f_output = theano.function(
        inputs=[x],
        outputs=dnn.output()
        )
        
    shared = [s_data, s_target, s_test_data, s_test_target, s_param_list]
        
    return f_train, f_training_error, f_test_error, f_output, dnn, shared
    
    
class VnnClf():
    def __init__(self,
            training_feature_vec, training_GT_vec,
            test_feature_vec, test_GT_vec,
            initial_params, param_name_list, activation_list,
            vector_activation_shape_list):
        """
        number of layers is limited to 1 or 2 if you use get_w_list()        
        
        Prameters
        ----------
        training_feature_vec : ndarray
            unnormalized feature_vec (n_pixels, n_tensors, dim)
        training_GT_vec : ndarray
            (n_pixels, dim)
        test_feature_vec, test_GT_vec
            unnormalized
        initial_params : list of ndarray
            [W_bottom, bias_bottom, W_2, bias_2, ..., W_top, bias_top]
            length // 2 is number of perceptrons
            W (in_dim, out_dim): ndarray
            b (out_dim, ) : ndarray
        param_name_list : list of str
            ["W_bottom", "bias_bottom", ..., "W_top", "bias_top"]
        activation_list : list of str
            ["ReLU",... "sigm"]
        vector_activation_shape_list : list of int
            [dim, ...dim]
        """
        
        n_layers = len(activation_list)
        n_tensors = training_feature_vec.shape[1]
        dim = training_feature_vec.shape[2]
        
        normed_training_feature_vec, norm_coef = TN_utils.normalize_feature_vec(training_feature_vec)
        normed_test_feature_vec = test_feature_vec / norm_coef[:, np.newaxis]
        
        training_input = normed_training_feature_vec.reshape(
            (normed_training_feature_vec.shape[0], n_tensors*dim))
        test_input = normed_test_feature_vec.reshape(
            (normed_test_feature_vec.shape[0], n_tensors*dim))
        
        f_train, f_training_error, f_test_error, f_output, dnn, shared = optimize_graph(
            training_input, training_GT_vec,
            test_input, test_GT_vec,
            initial_params, param_name_list, activation_list,
            drop_p=1., input_drop=1., vector_activation_shape_list=vector_activation_shape_list,
            n_layers=n_layers)
            
        
        self.n_pixels = training_feature_vec.shape[0]
        self._n_layers = n_layers
        self._n_tensors = n_tensors
        self._dim = dim
        
        self._f_train = f_train
        self._f_training_error = f_training_error
        self._f_test_error = f_test_error
        self._f_output = f_output
        
        self._dnn = dnn
        self._training_data = shared[0]
        self._training_target = shared[1]
        self._test_data = shared[2]
        self._test_target = shared[3]
        self._param_list = shared[4]
        self._norm_coef = norm_coef
        
    def train(self, i_batch, lr, regL2, batch_size):
        """
        Parameters
        ----------
        i_batch : int
        lr : float
        regL2 : float
        batch_size : float
        """
        self._f_train(i_batch, lr, regL2, batch_size)
        
    def training_error(self):
        return self._f_training_error()
        
    def test_error(self):
        return self._f_test_error()
        
        
    def output(self, feature_vec):
        """
        normalize and reshape feature_vec
        compute output
        
        Parameters
        ----------
        feature_vec : ndarray
            unnormalized (n_pixels, n_tensors, dim)
            
        Returns
        -------
        gt2vec : ndarray
            (n_pixels, dim)
        """
        normed_reshaped = (feature_vec / self._norm_coef[:, np.newaxis]).reshape(
                        (feature_vec.shape[0], self._n_tensors*self._dim))
        return self._f_output(normed_reshaped)
        
    
    def get_params(self):
        """
        Returns
        -------
        param_list : list of ndarray   
            [W_bottom, bias_bottom, W_2, bias_2, ..., W_top, bias_top]
            length // 2 is number of perceptrons
            W (in_dim, out_dim): ndarray
            b (out_dim, ) : ndarray
        
        """
        return [a.get_value(borrow=True) for a in self._param_list]
        
    def get_w_list(self):
        """
        Returns
        -------
        w2D_list : list of ndarray
            list of 2*2 array 
        """
        param_list = self.get_params()
        
        w1 = param_list[0]
        
        w2D_list = []
        for i in range(w1.shape[0] // 2):
            for j in range(w1.shape[1] // 2):
                w2D_list.append(w1[2*i : 2*i+2, 2*j : 2*j+2])
        
        if self._n_layers==2:
            w2 = param_list[2]
            for i in range(w2.shape[0] // 2):
                for j in range(w2.shape[1] // 2):
                    w2D_list.append(w2[2*i : 2*i+2, 2*j : 2*j+2])
        
        return w2D_list
        
    def initialize_params(self, param_list):
        """
        dnnはグラフのコンパイルが速いので、これ使わんくていい
        initialize theano.TensorSharedVariable params        
        
        Parameters
        ----------
        param_list : list of ndarray
            [W_bottom, bias_bottom, W_2, bias_2, ..., W_top, bias_top]
            length // 2 is number of perceptrons
            W (in_dim, out_dim): ndarray
            b (out_dim, ) : ndarray
            
        """
        for s_param, param in zip(self._param_list, param_list):
            s_param.set_value(param, borrow=False)
            
    def set_training(self, training_feature_vec, training_GT_vec):
        """
        dnnはグラフのコンパイルが速いので、これ使わんくてもいい
        set theano.TensorSharedVariable training_data, training_target
        
        Prameters
        ---------
        training_feature_vec : ndarray
            unnormalized feature_vec (n_pixels, n_tensors, dim)
        training_GT_vec : ndarray
            (n_pixels, dim)
            
        """
        
        normed_training_feature_vec, norm_coef = TN_utils.normalize_feature_vec(training_feature_vec)
        self._norm_coef = norm_coef
        
        training_input = normed_training_feature_vec.reshape(
            (normed_training_feature_vec.shape[0], self._n_tensors*self._dim))
            
        self.n_pixels = training_feature_vec.shape[0]
        
        
        self._training_data.set_value(training_input, borrow=True)
        self._training_target.set_value(training_GT_vec, borrow=True)
        
    def set_test(self, test_feature_vec, test_GT_vec):
        """
        dnnはグラフのコンパイルが速いので、これ使わんくてもいい
        Parameters
        ----------
        test_feature_vec, test_GT_vec
            unnormalized
        """
        
        normed_test_feature_vec = test_feature_vec / self._norm_coef[:, np.newaxis]
        test_input = normed_test_feature_vec.reshape(
            (normed_test_feature_vec.shape[0], self._n_tensors*self._dim))
        
        self._test_data.set_value(test_input, borrow=True)
        self._test_target.set_value(test_GT_vec, borrow=True)
        
    
            
        
            
        
            