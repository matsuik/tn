# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 15:56:00 2015

@author: matsuik
"""

from __future__ import division

import time

import numpy as np

import theano
import theano.tensor as T

def train(featureImg, imgGT, lr, mom, reg2, reg1, batch_size, n_epochs):
    """
    featureImg (n_pixels, n_tensor, 2)
    imgGT (n_pixels, 2)
    """
    
    # データの正規化
    norm_coef = np.std(np.linalg.norm(featureImg, axis=2), axis=0) # (60,)
    featureImg = featureImg / norm_coef[:, np.newaxis]
    
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True,
                            )
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "imgGT", borrow=True,
                             )
                             
    s_mom = theano.shared(mom)
    
    s_lr = theano.shared(lr)
    
    # 60個のパラメータ
    w = theano.shared(np.random.normal(scale=0.1, size=(n_tensor,)))
    # for momentum
    old_update = theano.shared(np.zeros(w.get_value(borrow=True).shape),
                               borrow=True)
    
    # 1つのデータ(batch_size, 60, 2) の代わり
    x_sy = T.tensor3("x")
    # １つのtarget (batch_size, 2)の代わり
    t_sy = T.matrix("y")
    
        
    h = T.tensordot(w, x_sy, axes=[0, 1]) # (batch_size, 2)

    
    gt2_h = [T.outer(h[i], h[i]) / (h[i].norm(L=2) + 0.01) for i in xrange(batch_size)]
    
    gt2_target = [T.outer(t_sy[i], t_sy[i]) / (t_sy[i].norm(L=2) + 0.01) for i in xrange(batch_size)]
    
    # L2_cost
    cost = T.sum([T.sum((agt2_h - agt2_target)**2) for agt2_h, agt2_target
        in zip(gt2_h, gt2_target)])
    obj = cost + reg2*w.norm(L=2) + reg1*w.norm(L=1)
    # momentum
    grads = T.grad(cost=obj, wrt=w)
    delta = -s_lr * grads + s_mom * old_update
    
    updates = [(w, w + delta),
               (old_update, delta)]
    
    index = T.iscalar("index")
    
    f_train  = theano.function(
        inputs=[index],
        outputs=[cost],
        updates=updates,
        givens=[(x_sy, s_input[index*batch_size: (index+1)*batch_size]),
                (t_sy, s_target[index*batch_size: (index+1)*batch_size])]
        )
        
    f_decay_lr = theano.function(
        inputs=[],
        updates=[(s_lr, s_lr*0.98)]
        )

    start_time = time.clock()
    cost_array = np.zeros((n_epochs,))
    
    print "training model ....!"
    
    for i_epoch in xrange(n_epochs):
        f_decay_lr()
        for i_batch in xrange(featureImg.shape[0] // batch_size):
            cost_i_pix = f_train(i_batch)
            cost_array[i_epoch] = cost_array[i_epoch] + cost_i_pix
        print i_epoch, cost_array[i_epoch]
        
    print "Training took", time.clock() - start_time, "sec"
    
    w = w.get_value()
    w = w / norm_coef
    return w, cost_array
    
    
                                        
