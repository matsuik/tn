# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 15:56:00 2015

@author: matsuik
"""

from __future__ import division

import numpy as np

import theano
import theano.tensor as T

def make_computaional_func(featureImg, imgGT, batch_size, w_comp=None):
    """
    featureImg (n_pixels, n_tensor, 2)
    imgGT (n_pixels, 2)
    """
    
    # データの正規化
    norm_coef = np.std(np.linalg.norm(featureImg, axis=2), axis=0) # (60,)
    featureImg = featureImg / norm_coef[:, np.newaxis]
    
    
    n_tensor = featureImg.shape[1]
    
    s_input = theano.shared(np.asarray(featureImg, dtype=theano.config.floatX),
                            "featureImg", borrow=True,
                            )
    s_target = theano.shared(np.asarray(imgGT, dtype=theano.config.floatX),
                             "gt2", borrow=True,
                             )
                             
    s_reg2 = T.scalar()
    s_reg1 = T.scalar()
    
    s_lr = T.scalar()
    
    if w_comp == None:
        # 60個のr
        magni = theano.shared(np.random.uniform(low=-0.01, high=0.01,
                                            size=(n_tensor, )))
        # 60個のtheta
        theta = theano.shared(np.random.uniform(low=-2*np.pi, high=2*np.pi,
                                            size=(n_tensor, )))
                                                
    else:
        magni = theano.shared(np.absolute(w_comp) * norm_coef)
        theta = theano.shared(np.angle(w_comp))
        
    bias = theano.shared(0.)
    
    
    # 1つのデータ (batch, 60, 2)の代わり
    x_sy = T.tensor3("x")
    # １つのtarget (batch, 2)の代わり
    t_sy = T.matrix("y")
    
    # ６０この回転伸縮行列
    w_mat_list = []
    for i in range(n_tensor):
        r = magni[i] * T.as_tensor([T.cos(theta[i]), -T.sin(theta[i]), T.sin(theta[i]), T.cos(theta[i])])
        r = r.reshape((2, 2))
        w_mat_list.append(r)
    
    # (batch, 2)
    h = T.sum([T.tensordot(x_sy[:,i], w_mat_list[i], axes=(1, 1)) for i in xrange(n_tensor)], axis=0)
    
    out = T.nnet.sigmoid(h.norm(L=2, axis=1) + bias).dimshuffle(0, "x") * h / (np.e**(-19) + h.norm(L=2, axis=1).dimshuffle(0, "x"))

    cost = T.sum((out - t_sy)**2)
    obj = cost + s_reg2*magni.norm(L=2) + s_reg1*magni.norm(L=1)
    
    grad_list = [T.grad(cost=obj, wrt=param) for param in (magni, theta, bias)]
    delta_list = [-s_lr / batch_size * grads for grads in grad_list]
    
    updates = [(magni, magni + delta_list[0]),
               (theta, theta + delta_list[1]),
                (bias, bias + delta_list[2])]
    
    index = T.iscalar("index")
    
    f_train  = theano.function(
        inputs=[index, s_lr, s_reg2, s_reg1],
        outputs=[cost],
        updates=updates,
        givens=[(x_sy, s_input[index : index+batch_size]),
                (t_sy, s_target[index : index+batch_size])]
        )
        
    f_output = theano.function(
        inputs=[x_sy],
        outputs=[out]
        )

        
    return f_train, f_output, magni, theta, bias, norm_coef
    
    
                                        
