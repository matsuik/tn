\expandafter\ifx\csname ifdraft\endcsname\relax


\documentclass[sotsuron]{kuee}

\usepackage{amsmath}
\usepackage{bm}
\usepackage[dvipdfmx]{graphicx}

\charsinline{36}
\linesinpage{32}

\begin{document}


\fi





\chapter{問題設定}


回転共変な学習機が役に立つ問題は自明ではないので、この章では、教師あり学習におけるターゲットベクトルと入力ベクトルが回転共変になるように定式化できる線分検出問題について説明する。与えられるデータは図\ref{fig:neuron_img}のスカラー画像と、線の真ん中のピクセルに割り当てられている図\ref{fig:neuron_GT}のground truthである。教師あり学習では、まず画像から特徴量を抽出し、パラメトライズされた関数に入力して、その出力とターゲットの誤差を計算する。そしてその誤差を最小化するようにパラメータを最適化する。次節から教師あり学習のターゲット(望ましい出力)、入力に用いる特徴量、パラメトライズされた関数、最適化手法について順に説明していく。

\begin{figure}[p]
\begin{center}
  \includegraphics[width=9cm]{../figures/neuron_img.png}
  \caption{与えられるスカラー画像}
  \label{fig:neuron_img}
\end{center}
\end{figure}

\begin{figure}[p]
\begin{center}
  \includegraphics[width=9cm]{../figures/neuron_GT.png}
  \caption{Ground Truth - 線構造の中心のピクセルに方向が与えられている}
  \label{fig:neuron_GT}
\end{center}
\end{figure}

教師あり学習の例として有名な手描き数字(MNISTなど)に対する分類問題との違いを説明しておく。MNISTの場合では、画像全体が一つのデータポイントに対応していて、画像１枚につき、輝度値を並べたピクセル数次元の特徴ベクトルと、クラスラベルを表すクラス数次元のターゲットベクトルが与えられる。本研究の問題では、ピクセル一つがデータポイント一つに対応している。ピクセル１つにつき、画像の勾配などを表す複数の２次元ベクトルが特徴ベクトルとして使われ、線の向きを表す一つの２次元ベクトルがターゲットベクトルとして与えられている。




\label{sec:target and features}
\section{ターゲットおよび, 入力に用いる特徴量}


この研究で扱う教師あり学習において、ターゲットベクトルが画像の回転と共変性をもっていること、また、ターゲットベクトルと共変性をもっているような特徴ベクトルが作れることを説明する。





\subsection{Ground Truth}


図\ref{fig:neuron_GT}のように、ground truthは線分の中心に対応するピクセルに割り当てられていて、線分の方向を表している。Ground truthは人手で作ったものである。ラインの中央にあるground truthのベクトルのノルムは全て等しく、それ以外の場所ではゼロベクトルになっている。
一つのピクセルにはgound truthとして, 線分の方向を表す２次元ベクトル\( \bm{v} \)が割り当てられている。
\begin{eqnarray}
\bm{v} &=& \left(
 \begin{array}{c}
  x \\
  y
 \end{array}
\right)
\end{eqnarray}


画像上のあるピクセルの位置を２次元ベクトル\( \bm{x} \)で表す。スカラー画像は\( \bm{x} \)上の関数として表せて、それを\( I(\bm{x}) \)と表す。各ピクセルに割り当てられた\( \bm{v} \)全体は\( \bm{v}[I](\bm{x}) \)と表す。画像\( I \)を\( \theta \)だけ回転させた画像を\( I_\theta \)をすると、以下の関係がある。
\begin{eqnarray}
I_\theta(\bm{x}) &=& I(R_\theta^T\bm{x}) \\
 \label{eq:eq1}
\bm{v}[I_\theta](\bm{x}) &=&  R_\theta \, \bm{v}[I](R_\theta^T\bm{x})\\
R_\theta &=& \left(
    \begin{array}{cc}
      \cos\theta & -\sin\theta \\
      \sin\theta & \cos\theta
    \end{array}
\right)
\end{eqnarray}
式(\ref{eq:eq1})は画像と線分の方向の関係から出てくるものである。\( \bm{v} \)は画像に対する\( \theta \)の回転に対して\( \theta \)だけ同じ方向に回転する回転共変な量になっている。

ここでは２次元ベクトルの回転を扱っているので、それを複素数で表すと便利になる。以下のように、回転行列を一つの複素数で表すことができる。
\begin{eqnarray}
\label{eq:copvec}
\left(
    \begin{array}{cc}
      \cos\theta & -\sin\theta \\
      \sin\theta & \cos\theta
    \end{array}
\right)
\left(
 \begin{array}{c}
  c \\
  d
 \end{array}
\right) \Leftrightarrow
e^{i\theta}(c + id)
\end{eqnarray}
\( \bm{v} \)を, \( v = x + iy \)のように複素数で表すと、回転共変性は以下のように書ける。
\begin{eqnarray}
v[I_\theta](\bm{x}) = e^{i\theta}v[I](R_\theta^T\bm{x})
\end{eqnarray}
\( v \)は線分の方向を表してるが、この問題では\( v \)と\( -v \)を区別する必要がないので、以下のように\( v \)も\( -v \)も同じ情報を表すように変換してからターゲットとして使う。
\begin{eqnarray}
gt = \frac{v^2}{||v||} = \frac{(-v)^2}{||-v||} \\
 = \frac{x^2 - y^2 + i2xy}{\sqrt{x^2+y^2}}
\end{eqnarray}
この\( gt \)は以下のように画像の回転に対して回転共変性を持っている。
\begin{eqnarray}
gt[I_\theta] &=& \frac{(v[I_\theta])^2}{||v[I_\theta]||} 
\\
&=& \frac{(e^{i\theta}v[I])^2}{||e^{i\theta} v[I]||}
\\
&=& \frac{e^{i2\theta}(v[I])^2}{||v[I]||}
\\ \label{eq: gt2ndtensor}
&=& e^{i2\theta}gt[I]
\end{eqnarray}

画像が\( \theta \)だけ回転した場合に\( \beta\theta \)だけ回転する量を\( \beta \)次のテンソルと呼ぶことにする。式(\ref{eq: gt2ndtensor})からわかるように、画像が\( \theta \)だけ回転すると\( gt\)は\( 2\theta \)だけ回転するので、\( gt \)は2次のテンソルである。\( gt \)はそもそも線の向きを表しているので、線の向きとも共変性がある。線の向きが\( \theta \)だけ傾くと、\( gt \)も\( 2\theta \)だけ回転する。




\label{sec:Rotation covariant features}
\subsection{回転共変な特徴量}


本研究で提案する回転共変な学習機では、入力と出力が同じだけ回転するので、ターゲットが２次のテンソルである以上、入力も2次のテンソルであると便利になる。本節では画像から２次のテンソルになっている特徴量をつくる方法を概説する。
本研究の応用例ではデータを２次元画像に限っているので、２次元ベクトル空間での回転行列と複素数の対応を活用できる。ここからは、画像から複素数で特徴量を作った後、それを2次元ベクトルで置き換える。

画像の複素数による\( k \)階微分を次のように定義する\cite{comp der}。
\begin{eqnarray}
a^{(k)}[I](\bm{x}) = \frac{1}{2}(\partial_{x_1} + \partial_{x_2})^k I(\bm{x})
\end{eqnarray}
どんな\( k \)においても\( a^{(k)}[I](\bm{x}) \)は\( k \)階微分の情報を含んでいるが、１つの複素数で表せる。3D以上で複素数が使えない場合は, 例えば1階微分はベクトル、２階微分ならヘッセ行列となって複雑になる。
\( \theta \)だけ回転した画像に対する\( k \)階微分は、回転する前の微分と以下の関係がある。
\begin{eqnarray}
a^{(k)}[I_\theta](\bm{x}) = e^{ik\theta}a^{(k)}[I](R_\theta^T\bm{x})
\end{eqnarray}
\(e^{ik\theta}\)が\( k\theta \)だけの回転を表しているので、\(a^{(k)}[I](\bm{x})\)は\( k \)次のテンソルである。しかし、学習機のターゲットである\( gt \)が２次のテンソルなので、\( k = 2 \)以外のこれらの\( k \)次のテンソル をそのまま入力として使うことはできない。

そこで、異なる次数のテンソルである複素数の積をとって、新しい次数のテンソルを作る。たとえば以下のように1階微分と３階微分から２次のテンソルを作ることができる。	
\begin{eqnarray}
b[I] &=& a^{(3)}[I](a^{(1)}[I])^* \\
b[I_\theta] &=& e^{i3\theta}a^{(3)}[I]e^{-i\theta}(a^{(1)}[I])^* = e^{i2\theta}b[I]
\end{eqnarray}
本研究の実験では、\( k = 1, \cdots, 5 \)として、\(a^{(k)}[I](\bm{x})\)を3つずつ組み合わせることで2次のテンソルを12個作った。たとえば、\( b^{(2)} = a^{(1)}a^{(5)}\overline{a^{(4)}} \)などとした。図\ref{fig:neuron_features}に様々な２次のテンソルを示した。これらの特徴を\( b^{(2)}_i[I](\bm{x}), \ i=1,\cdots,12 \)と表す。３個ずつの組み合わせでこれ以上２次のテンソルを作っても、等価なものができてしまうため意味がない。

\begin{figure}[p]
\begin{center}
  \includegraphics[width=15cm]{../figures/neuron_features.png}
  \caption{\( k = 1, \cdots, 5 \)として、\(a^{(k)}[I](\bm{x})\)を3つずつ組み合わせることで2次のテンソルを12個作った}
  \label{fig:neuron_features}
\end{center}
\end{figure}

最後にこれらの特徴量と\( gt \)を2次元ベクトルに変換してから、教師あり学習に使う。今は２次元なので複素数のままでも扱えるが、3D以上の場合への拡張性を考慮して実数ベクトルとして扱う。式(\ref{eq:copvec})の関係があるので、単純に下のように変換すれば\( \bm{b}^{(2)}[I](\bm{x}) \)も式(\ref{eq: 2ndtensor})のように２次のテンソルになっている。\( gt \)も同じように2次元ベクトルとして\( \bm{gt} \)と表記する。

\begin{eqnarray}
\bm{b}^{(2)}_i[I](\bm{x}) = \left(
 \begin{array}{c}
  \mathrm{Re} \left( b^{(2)}_i[I](\bm{x}) \right)\\
  \mathrm{Im} \left( b^{(2)}_i[I](\bm{x}) \right)
 \end{array}
\right) \\
\label{eq: 2ndtensor}
\bm{b}^{(2)}_i[I_\theta](\bm{x}) = R_{2\theta} \, \bm{b}^{(2)}_i[I](R_\theta^T\bm{x})
\end{eqnarray}

\( \bm{b}^{(2)}_i[I](\bm{x}) \)は線の向きとも共変性を持っている場合が多い。たとえば水平方向の線分と、45度傾いた線分を考える。45度傾いた線分付近の画像は、水平方向の線分付近の画像を回転したものとみなせるので、\( \bm{b}^{(2)}_i \)は\( R_{2\theta} \)回転していると想定できる。ただの線分になっている部分と、線が交差している部分や分岐している部分では共変性はない。

結局、ターゲットベクトルと入力に使う特徴ベクトルが回転共変性を持つとみなせる。次章で説明するように、入力と出力が回転共変な関数をもちいて教師あり学習を行うことで、ターゲットベクトルと入力ベクトルの回転共変性を活用することができる。



\expandafter\ifx\csname ifdraft\endcsname\relax
  \end{document}
\fi


