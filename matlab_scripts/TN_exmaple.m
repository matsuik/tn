


%% Rotation of a tensor field

    close all
    
    %loading the image
     img=imread('neuron01.png');
     img=single(img)/255;
     figure(1);
     imshow(img);

    %we are going to rotate derivatives of order 5
    degree=45;
    order=5;
    
    %smoothing the image
    imgs=TN_smooth(img,3);
    
    %computing first order derivative
    Ia=TN_complex_derivative(imgs);
    
    %computing first order derivative of a rotated version of img
    Ira=TN_complex_derivative(imrotate(imgs,degree,'bicubic'));
    
    %computing the higher order derivatives
    for o=2:order
        Ia=TN_complex_derivative(Ia);
        Ira=TN_complex_derivative(Ira);
     end; 
     
     % computing the value transform of the tensors 
     % and rotate the tensor field Ia
     phi=degree*pi*2/360;
     value_transform=exp(order*1i*phi);
     Iar=value_transform*imrotate(Ia,degree);
     
     
     %now we have two rotated tensor fields
     %Ira: rotate the image => compute derivatives
     %Iar: compute derivatives=> rotate the tensor field
     %both are identical (up to numerical issues)
     
     
     figure(4);
     clf;
     hold on;
     subplot(2,2,1)
     imagesc(real(Ira))
     subplot(2,2,2)
     imagesc(real(Iar))
     subplot(2,2,3)
     imagesc(imag(Ira))
     subplot(2,2,4)
     imagesc(imag(Iar))        
     colormap gray



%% Training and Applying a Tensor Network
 hold on
 %loading the image
 img=imread('../data/neuron01.png');
 img=single(img)/255;
 imshow(img);
 
 
 %loading the ground truth
 load ../data/neuron01_GT.mat
 Vgt=TN_makeGT(img,GT);
 figure;TN_plot(img,Vgt,1,2);
 
 [product_rule, M] = TN_product_rule();
 
 [featureImg, M] = TN_b(img, product_rule, M);
 
 imgGT = TN_imgGT(Vgt, M);
 
 %traing the network
 [Vtrain,M] = TN_filter_train(featureImg, imgGT, M);
 figure;TN_plot(img,Vtrain,1,2);
 %M is the network model
 
 %loading a new image
 img2=imread('neuron03.png');
 img2=single(img2)/255;
 
 [featureImg2, M, a] = TN_b(img2, product_rule, M);
 
 %applying the network to the new image
 Vtest = TN_filter_apply(featureImg2,M);
 figure;TN_plot(img2,Vtest,1,2);
 
 
 hold off