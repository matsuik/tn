% toytreeのオリジナルの画像、Vgt(see TN_example.)
load ../data/toytree.mat
load ../data/toytree_img_noise.mat
figure;TN_plot(imgn(:, 1:256), Vgt(:, :, 1:256), 1, 2);
saveas(gcf, '../results/toytree_confirmRC/trainingGT.fig')

load ../results/toytree_confirmRC/v_train.mat
figure;TN_plot(Img(:, 1:256), v_train, 1, 2);
saveas(gcf, '../results/toytree_confirmRC/training_prediction.fig')

% toytreeを10度ずつ３６回回したものの画像
load ../data/toytreerot_img.mat
load ../results/toytree_confirmRC/v_test.mat
figure;TN_plot(squeeze(imrs(12, :, 1:256)), v_test, 1, 2);
saveas(gcf, '../results/toytree_confirmRC/test_prediction.fig')