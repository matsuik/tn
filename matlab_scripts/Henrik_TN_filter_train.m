function [V,M] = TN_filter_train(img,Vgt)
            
            shape=size(img);
            scales=[1:5];
            max_order=5;
            regu=0.001;
            epsilon=0.01;

            for s=1:numel(scales)
                %optionally: local standard normalization factor
                std_dev=TN_std(img,scales(s)*1.5,'inv',false);
                
                % the Gausian filtered image
                img_smoothed=TN_smooth(img,scales(s),'normalize',true);
                
                
                
                a{s}=zeros([max_order,shape]);
                
                % first order complex derivative
                a{s}(1,:,:)=TN_complex_derivative(img_smoothed);
                
                % remaining derivatives
                for o=2:max_order
                    a{s}(o,:,:)=TN_complex_derivative(squeeze(a{s}(o-1,:,:)));
                end;
                
                % normalizing with respect to local intensity variation
                for o=1:max_order
                    a{s}(o,:,:)=squeeze(a{s}(o,:,:))./(std_dev+epsilon);
                end;
            end;
            

            % the GT is given as vector, but actually it is second order
            % tensor
            imgGT=squeeze(Vgt(1,:,:))+1i*squeeze(Vgt(2,:,:));
            imgGT=imgGT.*imgGT./(abs(imgGT)+eps);

            

            %we now create third order tensors from the derivative tensors
            %by conbining up to two tensors
            order1=false;
            order2=false;
            order3=true;
            products=[];

            if order1
                products=[products;[2,0,0,0]];
            end;
            
            
            if order2            
                        
                        for o1=1:max_order
                            for o2=1:max_order
                                if (o1+o2)==2
                                    products=[products;[o1,o2,+1,0]];
                                end;
                                if (o1-o2)==2
                                    products=[products;[o1,o2,-1,0]];
                                end;
                            end;
                        end;

 
            end;
            if order3
                         products3=[];
                        for o1=1:max_order
                            for o2=1:max_order
                                for o3=1:max_order
                                    if ((o1-o2)>-1)&&((o1-o2-o3)==2)
                                        products3=[products3;[o1,o2,o3,1]];
                                    end;
                                    if (o1>=o2) && (o2>=o3) &&((o1+o2+o3)==2)
                                        products3=[products3;[o1,o2,o3,2]];
                                    end;
                                    if (o1+o2-o3)==2
                                        products3=[products3;[o1,o2,o3,3]];
                                    end;
                                end;
                            end;
                        end;

                        D=(distmat(sort(products3(:,1:3),2),sort(products3(:,1:3),2)));
                        [C,ia,ic]=unique(sort(products3(:,1:3),2),'rows');
                        products=[products;products3(ia,:)];
                        
            end;
            
             num_tensors_per_scale=(size(products,1)); 
             featureImg=zeros([prod(shape),num_tensors_per_scale*numel(scales)]);
                count=1;
                for s=1:numel(scales)
                    for p=1:size(products,1)
                        if (products(p,2)==0)
                            featureImg(:,count)=a{s}(2,:);
                        elseif (products(p,4)==0)
                                if (products(p,end)>0)
                                    featureImg(:,count)=a{s}(products(p,1),:).*a{s}(products(p,2),:);
                                else
                                    featureImg(:,count)=a{s}(products(p,1),:).*conj(a{s}(products(p,2),:));
                                end;
                        else
                            switch products(p,end)
                                case 1
                                    featureImg(:,count)=a{s}(products(p,1),:).*conj(a{s}(products(p,2),:)).*conj(a{s}(products(p,3),:));
                                case 2
                                    featureImg(:,count)=a{s}(products(p,1),:).*a{s}(products(p,2),:).*a{s}(products(p,3),:);
                                case 3
                                    featureImg(:,count)=a{s}(products(p,1),:).*a{s}(products(p,2),:).*conj(a{s}(products(p,3),:));
                            end;
                        end;
                        count=count+1;
                    end;
                end;
             
             

                
            if true            
                        % solving lenar system (we select only pixels with signal)
                        std_dev=TN_std(img,5,'inv',false);
                        indx=squeeze(abs(TN_smooth(img,6)./(std_dev+0.1)))>0.25;

                        nVcol=sum(indx(:));
                        renormfac = sqrt(sum(abs(featureImg(indx,:)).^2,1)/nVcol)';
                        invrenormfac = 1./renormfac;
                        featureImg2 = featureImg(indx,:).* (invrenormfac*ones(1,nVcol))';
                        Corr = featureImg2'*featureImg2;
                        b = featureImg2'*imgGT(indx);
                        W = (Corr+regu*diag(invrenormfac.^2))\b;
                        W=invrenormfac.*W;

                        Vo2=((featureImg(indx,:))*W);
                        Vo1=sqrt(Vo2);
                        V=zeros([2,shape]);
                        V(1,indx)=real(Vo1);
                        V(2,indx)=imag(Vo1);

            else
                        % solving lenar system (all pixels)
                        nVcol=prod(shape);
                        renormfac = sqrt(sum(abs(featureImg(:,:)).^2,1)/nVcol)';
                        invrenormfac = 1./renormfac;
                        featureImg2 = featureImg(:,:).* (invrenormfac*ones(1,nVcol))';
                        Corr = featureImg2'*featureImg2;
                        b = featureImg2'*imgGT(:);
                        W = (Corr+regu*diag(invrenormfac.^2))\b;
                        W=invrenormfac.*W;
                        Vo2=reshape(((featureImg(:,:))*W),shape);
                        Vo1=sqrt(Vo2);
                        V=zeros([2,shape]);
                        V(1,:,:)=real(Vo1);
                        V(2,:,:)=imag(Vo1);
            end;

            M.W=W;                      % the weight matrix
            M.max_order=max_order;      
            M.products=products;        % product rules
            M.scales=scales;
            M.epsilon=epsilon;
            