function [V,M] = TN_filter_train(featureImg, imgGT, M)
           
            shape = M.shape;
            regu=0.001;

            % solving lenar system (all pixels)
            nVcol=prod(shape);
            
           
            renormfac = sqrt(sum(abs(featureImg(:,:)).^2,1)/nVcol)';
            invrenormfac = 1./renormfac;
            
            %　正規化
            featureImg2 = featureImg(:,:).* (invrenormfac*ones(1,nVcol))';
            Corr = featureImg2'*featureImg2;
            b = featureImg2'*imgGT(:);
            
            % 正規化したfeatureでのW
            W = (Corr+regu*diag(invrenormfac.^2))\b;
            
            % 正規化をもどす
            W=invrenormfac.*W;
            
            % 正規化してないfeatureにたいしてapply
            Vo2=reshape(((featureImg(:,:))*W),shape);
            Vo1=sqrt(Vo2);
            V=zeros([2,shape]);
            V(1,:,:)=real(Vo1);
            V(2,:,:)=imag(Vo1);
                        
            M.W=W;                      % the weight matrix
            