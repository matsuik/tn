function imgGT = TN_imgGT(Vgt, M)
%imgGT: shape(1)xshape(2) complex double
            eps = M.epsilon;
            % the GT is given as vector, but actually it is second order
            % tensor
            imgGT=squeeze(Vgt(1,:,:))+1i*squeeze(Vgt(2,:,:));
            imgGT=imgGT.*imgGT./(abs(imgGT)+eps);