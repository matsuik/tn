function Vgt=TN_makeGT(Img,A)

    shape=size(Img);
    
    Vgt=zeros([2,shape]);

    posSub=round(A.data(1:2,:))+1;
    
    posI=sub2ind(shape,posSub(1,:),posSub(2,:));
    
    
    directions=(A.data(4:5,:));
    directions(:,:)=directions(:,:)./(repmat((sqrt(sum(directions(:,:).^2,1))),2,1)+eps);
    
    Vgt(:,posI)=directions;
    