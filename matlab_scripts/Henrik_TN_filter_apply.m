function V = TN_filter_apply(img,M)
            
            shape=size(img);
            scales=M.scales;
            max_order=M.max_order;


            
            for s=1:numel(scales)
                std_dev=TN_std(img,scales(s)*1.5,'inv',false);
                
                img_smoothed=TN_smooth(img,scales(s),'normalize',true);
                
                a{s}=zeros([max_order,shape]);
                a{s}(1,:,:)=TN_complex_derivative(img_smoothed);
                for o=2:max_order
                    a{s}(o,:,:)=TN_complex_derivative(squeeze(a{s}(o-1,:,:)));
                end;
                for o=1:max_order
                    a{s}(o,:,:)=squeeze(a{s}(o,:,:))./(std_dev+M.epsilon);
                end;
            end;

            
            tmp=zeros([1,shape]); 
            products=M.products;
            
                count=1;
                for s=1:numel(scales)
                    for p=1:size(products,1)
                        if (products(p,2)==0)
                            tmp=tmp+M.W(count)*a{s}(2,:,:);
                        elseif (products(p,4)==0)
                                if (products(p,end)>0)
                                    tmp=tmp+M.W(count)*a{s}(products(p,1),:,:).*a{s}(products(p,2),:,:);
                                else
                                    tmp=tmp+M.W(count)*a{s}(products(p,1),:,:).*conj(a{s}(products(p,2),:,:));
                                end;
                        else
                            switch products(p,end)
                                case 1
                                    tmp=tmp+M.W(count)*a{s}(products(p,1),:,:).*conj(a{s}(products(p,2),:,:)).*conj(a{s}(products(p,3),:,:));
                                case 2
                                    tmp=tmp+M.W(count)*a{s}(products(p,1),:,:).*a{s}(products(p,2),:,:).*a{s}(products(p,3),:,:);
                                case 3
                                    tmp=tmp+M.W(count)*a{s}(products(p,1),:,:).*a{s}(products(p,2),:,:).*conj(a{s}(products(p,3),:,:));
                            end;
                        end;
                        count=count+1;
                    end;
                end;

            
            V=zeros([2,shape]);
            tmp=sqrt(tmp);
            V(1,:,:)=V(1,:,:)+real(tmp);
            V(2,:,:)=V(2,:,:)+imag(tmp);                                
            
            


            
            