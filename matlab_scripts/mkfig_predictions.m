% V1
load ../results/CondNumVLR_biasedReLU/v_train360_list.mat
load ../data/toytreerot_img.mat

figure;TN_plot(squeeze(imrs(1, 70:115, 145:190)), squeeze(v_train360_list(1,:,70:115,145:190)), 1, 2);
axis('equal')
axis('off')
set(gca, 'LooseInset', get(gca, 'TightInset'));
saveas(gcf, '../figures/V1train.png')


load ../results/CondNumVLR_biasedReLU/v_train120_list.mat
figure;TN_plot(squeeze(imrs(12, 160:205, 125:170)), squeeze(v_train120_list(1,:, 160:205,125:170)), 1, 2);
axis('equal')
axis('off')
set(gca, 'LooseInset', get(gca, 'TightInset'));
saveas(gcf, '../figures/V1test.png');


%S1
load ../results/CondNumVLRscalaract/v_train360_list.mat
load ../data/toytreerot_img.mat

figure;TN_plot(squeeze(imrs(1, 70:115, 145:190)), squeeze(v_train360_list(1,:,70:115,145:190)), 1, 2);
axis('equal')
axis('off')
set(gca, 'LooseInset', get(gca, 'TightInset'));
saveas(gcf, '../figures/S1train.png')


load ../results/CondNumVLRscalaract/v_train120_list.mat
figure;TN_plot(squeeze(imrs(12, 160:205, 125:170)), squeeze(v_train120_list(1,:, 160:205,125:170)), 1, 2);
axis('equal')
axis('off')
set(gca, 'LooseInset', get(gca, 'TightInset'));
saveas(gcf, '../figures/S1test.png');


%RC2
load ../data/toytreerot_img.mat
load ../results/toytree12/v_train.mat
figure;TN_plot(squeeze(imrs(1, 70:115, 145:190)), v_train(:, 70:115,145:190), 1, 2);
axis('equal')
axis('off')
set(gca, 'LooseInset', get(gca, 'TightInset'));
saveas(gcf, '../figures/RC2train.png')

load ../data/toytreerot_img.mat
load ../results/toytree12/v_test.mat
figure;TN_plot(squeeze(imrs(12, 160:205, 125:170)), v_test(:, 160:205,125:170), 1, 2);
axis('equal')
axis('off')
set(gca, 'LooseInset', get(gca, 'TightInset'));
saveas(gcf, '../figures/RC2test.png')