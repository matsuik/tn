load ../results/CondNumVLR/v_train360_list.mat
load ../data/toytreerot_img.mat

figure;TN_plot(squeeze(imrs(1, :, 1:256)), squeeze(v_train360_list(1,:,:,:)), 1, 2);
saveas(gcf, '../results/CondNumVLR/train360_t1.fig')
figure;TN_plot(squeeze(imrs(1, :, 1:256)), squeeze(v_train360_list(3,:,:,:)), 1, 2);
saveas(gcf, '../results/CondNumVLR/train360_t36.fig')

load ../results/CondNumVLR/v_train120_list.mat
figure;TN_plot(squeeze(imrs(12, :, 1:256)), squeeze(v_train120_list(1,:,:,:)), 1, 2);
saveas(gcf, '../results/CondNumVLR/test120_t1.fig')
figure;TN_plot(squeeze(imrs(12, :, 1:256)), squeeze(v_train120_list(3,:,:,:)), 1, 2);
saveas(gcf, '../results/CondNumVLR/test120_t36.fig')
