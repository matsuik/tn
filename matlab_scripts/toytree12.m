
load ../data/toytreerot_img.mat
load ../results/toytree12/v_train.mat
figure;TN_plot(squeeze(imrs(1, :, 1:256)), v_train, 1, 2);
saveas(gcf, '../results/toytree12/training_prediction.fig')

% toytreeを10度ずつ３６回回したものの画像
load ../data/toytreerot_img.mat
load ../results/toytree12/v_test.mat
figure;TN_plot(squeeze(imrs(12, :, 1:256)), v_test, 1, 2);
saveas(gcf, '../results/toytree12/test_prediction.fig')