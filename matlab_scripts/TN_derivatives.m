% computes the Hessian matrix
% and optinally the gradient of an image

function [H,G]=TN_derivatives(Img)

    classname=class(Img);

    filter_mode='replicate';       
    

    if nargout>1
        kernel = zeros(3,3);
        kernel(1,2) = -1;  kernel(3,2) = 1;
        G=zeros([3,size(Img)],classname);
        G(1,:,:) = imfilter(Img,kernel,filter_mode);
        G(2,:,:) = imfilter(Img,permute(kernel,[2 1]),filter_mode);
    end;    
    
    
    
    H=zeros([3,size(Img)],classname);
    
    kernel = zeros(3,3);
    kernel(2,2) = -2; kernel(1,2) = 1; kernel(3,2) = 1;
    H(1,:,:) = imfilter(Img,kernel,filter_mode);
    H(2,:,:) = imfilter(Img,permute(kernel,[2 1]),filter_mode);
    kernel = zeros(3,3);
    kernel(1,1) = 0.25; kernel(3,3) = 0.25; kernel(1,3) = -0.25; kernel (3,1) = -0.25; 
    H(3,:,:) = imfilter(Img,kernel,filter_mode);    
    
    