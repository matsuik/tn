function index = TN_mask(img, thresh)
    std_dev=TN_std(img,6,'inv',false);
    index = squeeze(abs(TN_smooth(img,6)./(std_dev+0.1)))>thresh;